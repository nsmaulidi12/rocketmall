import Vue from 'vue'
import '~/node_modules/vue-slick-carousel/dist/vue-slick-carousel.css'
import VueSlickCarousel from 'vue-slick-carousel'

Vue.component('VueSlickCarousel', VueSlickCarousel)
