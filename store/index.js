export const state = () => ({
	isShowSearch: false,
	isShowCategory: false
})

export const mutations = {
	toggleSearch(state) {
		state.isShowSearch = !state.isShowSearch
		state.isShowCategory = false
	},
	toggleCategory(state) {
		state.isShowCategory = !state.isShowCategory
		state.isShowSearch = false
	}
}
